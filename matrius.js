let rows = document.querySelector("tbody").children
let matrix = []
for (var i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}

/*

matrix = [[2,3,4],
          [1,2,3],
          [4,5,6]];
*/



function paintAll() {
   erase();
    for (let i =0;i < matrix.length ; i++) { // files
        for (let j = 0; j < matrix[i].length ;j++ ) { // columnes
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function erase() {
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = 0 ;j < matrix[i].length; j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

function paintRightHalf() {
    erase();
    for (let i= 0; i < matrix.length; i++ ) { // afegir codi
        for (let j = Math.round(matrix.length/2); j < matrix[i].length ; j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}


function paintLeftHalf() {
    erase();

    for (let i= 0; i < matrix.length; i++) { // afegir codi
        for (let j= 0; j < matrix[i].length/2; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperHalf() {
    erase();

    for (let i = 0; i < matrix.length/2; i++) { // afegir codi
        for (let j = 0; j < matrix[i].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintLowerTriangle() {
    erase();

    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = 0; j < i; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperTriangle() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = i; j < matrix[i].length; j++) { // afegir codi
            // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintPerimeter() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = 0; j < matrix[i].length; j+=4) { // afegir codi
          if(i == 0 || i == matrix.length-1){
            for (let j = 0; j < matrix[i].length; j++) {
                matrix[i][j].style.backgroundColor = "red"; 
            }
                
          }
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintCheckerboard() {
    erase();
 
    for (let i = 0; i < matrix.length; i+=2) { // afegir codi
        for (let j = 0; j < matrix[i].length; j+=2) { // afegir codi
   
            matrix[i][j].style.backgroundColor = "red";
        }
    }
    for (let y = 1; y < matrix.length; y+=2) {
        for (let e = 1; e < matrix[y].length; e+=2) {
            matrix[y][e].style.backgroundColor = "red";
        }
    }
}




function paintCheckerboard2() {
    erase();
    for (let i = 0; i < matrix.length; i+=2) { // afegir codi
        for (let j = 1; j < matrix[i].length; j+=2) { // afegir codi
   
            matrix[i][j].style.backgroundColor = "red";
        }
    }
    
    for (let y = 1; y < matrix.length; y+=2) {
        for (let e = 0; e < matrix[y].length; e+=2) {
            matrix[y][e].style.backgroundColor = "red";
        }
    }

}

function paintNeighbours() {
    let inputX = document.getElementById("inputX").valueAsNumber;
    let inputY = document.getElementById("inputY").valueAsNumber;
    erase();
    for (let i = inputX-1; i <= inputX+1; i++) {
        for (let j = inputY-1; j <= inputY+1; j++) {
            try {
                
            matrix[i][j].style.backgroundColor = "red";
            
            if(matrix[inputX][inputY]) {

            for (let i = inputX; i < matrix.length; i++) {
                for (let j = inputY; j < matrix[i].length; j++) {
                    matrix[inputX][inputY].style.backgroundColor = "white";
                    }
                }

            }
            
            } catch {

            }
        }
    }

    
    }
    
        
            
        
function countNeighbours(x,y) {
 
    let count = 0;
  
    for (let i = x-1; i<= x+1; i++ ) {
        for (let j = y-1; j<= y+1; j++) {
            try {
                if(i==x && j==y){

                }
            else if (matrix[i][j].style.backgroundColor == "red") {
                count++;
            }
            } catch {

            }
        }
    }
            
     return count; 
}

function paintAllNeighbours() {
    
    for (let i =0; i < matrix.length ; i++) { // files
        for (let j = 0; j < matrix[i].length; j++ ) { // columnes
         count = countNeighbours(i,j); 
	    matrix[i][j].innerText = count;
	}
    }
    
}
